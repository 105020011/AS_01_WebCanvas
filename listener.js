document.getElementById("pencil").addEventListener('click', function() {
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasPencil = true;
    canvas.addEventListener('mousedown', pencilStart);
    canvas.addEventListener('mouseup', pencilStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth838.cur), auto"
}, false);

document.getElementById("txt").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasText = true;
    canvas.addEventListener('mousedown', doText, false);
    canvas.style.cursor = "text";
      
}, false);

document.getElementById("rectangle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasRec = true;
    canvas.addEventListener('mousedown', recStart);
    canvas.addEventListener('mouseup', recStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);

document.getElementById("circle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }  
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasCir = true;
    canvas.addEventListener('mousedown', cirStart);
    canvas.addEventListener('mouseup', cirStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);

document.getElementById("triangle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }  
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasTri = true;
    canvas.addEventListener('mousedown', triStart);
    canvas.addEventListener('mouseup', triStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);

document.getElementById("orectangle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
    console.log("click orec")
    hasORec = true;
    canvas.addEventListener('mousedown', orecStart);
    canvas.addEventListener('mouseup', orecStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);

document.getElementById("ocircle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOTri){
      canvas.removeEventListener('mousedown', otriStart);
      canvas.removeEventListener('mouseup', otriStop);
      hasOTri = false;
    }
  
    hasOCir = true;
    canvas.addEventListener('mousedown', ocirStart);
    canvas.addEventListener('mouseup', ocirStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);

document.getElementById("otriangle").addEventListener('click', function() {
    if(hasPencil){
      canvas.removeEventListener('mousedown', pencilStart);
    
      canvas.removeEventListener('mouseup', pencilStop);
      hasPencil = false;
      console.log("remove pencil");
    }
  
    if(hasText){
      canvas.removeEventListener('mousedown', doText);
      hasText = false;
      console.log("remove txt");
    }
  
    if(hasRec){
      canvas.removeEventListener('mousedown', recStart);
      canvas.removeEventListener('mouseup', recStop);
      hasRec = false;
    }
  
    if(hasCir){
      canvas.removeEventListener('mousedown', cirStart);
      canvas.removeEventListener('mouseup', cirStop);
      hasCir = false;
    }
  
    if(hasTri){
      canvas.removeEventListener('mousedown', triStart);
      canvas.removeEventListener('mouseup', triStop);
      hasTri = false;
    }
  
    if(hasORec){
      canvas.removeEventListener('mousedown', orecStart);
      canvas.removeEventListener('mouseup', orecStop);
      hasORec = false;
    }
  
    if(hasOCir){
      canvas.removeEventListener('mousedown', ocirStart);
      canvas.removeEventListener('mouseup', ocirStop);
      hasOCir = false;
    }
  
    hasOTri = true;
    canvas.addEventListener('mousedown', otriStart);
    canvas.addEventListener('mouseup', otriStop);
    canvas.style.cursor = "url(http://ani.cursors-4u.net/games/gam-16/gam1535.cur), auto";
      
}, false);