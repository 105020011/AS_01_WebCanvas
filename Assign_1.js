var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');

var hasInput=false;
var hasText=false;
var hasPencil=false;
var hasRec=false;
var hasTri=false;
var hasCir=false;
var hasORec=false;
var hasOTri=false;
var hasOCir=false;
var Tsize = "20px";
var Tfont = "sans-serif";
var startX;
var startY;
var beforeRec;
var beforeCir;
var beforeTri;
var savedI=-1;
var clr;

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
  console.log(mousePos.x,mousePos.y);
}

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  cPush();
}, false);

var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'black', 'white'];
var size = [1, 3, 5, 10, 15, 20];
var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];

function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    ctx.strokeStyle = colors[i];
    clr = colors[i];
    if(hasPencil){
    if(clr=='red'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth845.cur), auto";
    }else if(clr=='blue'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth839.cur), auto";
    }else if(clr=='green'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth837.cur), auto";
    }else if(clr=='purple'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth844.cur), auto";
    }else if(clr=='yellow'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth846.cur), auto";
    }else if(clr=='orange'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth842.cur), auto";
    }else if(clr=='black'){
      canvas.style.cursor = "url(http://ani.cursors-4u.net/others/oth-9/oth838.cur), auto";
    }else if(clr=='white'){
      canvas.style.cursor = "url(http://cur.cursors-4u.net/cursors/cur-11/cur1046.cur), auto";
    }
  }
  }, false);
}

var slider = document.getElementById("pencilSize");
ctx.lineWidth = slider.value;

slider.oninput = function() {
  ctx.lineWidth = slider.value;
}

for(var i = 0; i < colors.length; i++) {
  listener(i);
}

document.getElementById("sizeList").addEventListener('click', function() {
  console.log("change size");
  var e = document.getElementById("sizeList");
  var tmp_size = e.options[e.selectedIndex].value;
  Tsize = tmp_size + 'px';
  console.log(Tfont);
  ctx.font = Tsize + " " + Tfont;
  console.log(ctx.font);
    
}, false);

document.getElementById("fontList").addEventListener('click', function() {
  console.log("change font");
  var e = document.getElementById("fontList");
  var tmp_font = e.options[e.selectedIndex].value;
  Tfont = tmp_font;
  console.log(Tfont);
  ctx.font = Tsize + " " + Tfont;
  console.log(ctx.font);
    
}, false);

function addInput(x, y) {

  var input = document.createElement('input');

  input.type = 'text';
  input.style.position = 'fixed';
  input.style.left = (x - 4) + 'px';
  input.style.top = (y - 4) + 'px';

  input.onkeydown = handleEnter;

  document.body.appendChild(input);

  input.focus();

  hasInput = true;
}

function handleEnter(e) {
  var keyCode = e.keyCode;
  if (keyCode === 13) {
      drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
      document.body.removeChild(this);
      hasInput = false;
  }
}

function drawText(txt, x, y) {
  ctx.textBaseline = 'top';
  ctx.textAlign = 'left';
  ctx.font = Tsize + " " + Tfont;
  ctx.fillStyle = clr;
  console.log(ctx.font);
  ctx.fillText(txt, x - 4, y - 4);
  cPush();
}

function pencilStart(evt){
  console.log("start pencil");
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(mousePos.x, mousePos.y);
  evt.preventDefault();
  canvas.addEventListener('mousemove', mouseMove, false);
}

function pencilStop(){
  console.log("end pencil");
  canvas.removeEventListener('mousemove', mouseMove, false);
  cPush();
}

function doText(e){
  console.log("start txt");
  e.preventDefault();
  if(!hasInput){
    addInput(e.clientX, e.clientY);
  }
}

function recStart(evt){
  console.log("start rec");
  beforeRec = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add rec");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', recMove, false);
}

function recMove(evt) {
  
  console.log("rec move");
  ctx.putImageData(beforeRec, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.rect(startX, startY, mousePos.x-startX, mousePos.y-startY);
  ctx.fillStyle = clr;
  ctx.fill();
}

function recStop(){
  console.log("end rec");
  canvas.removeEventListener('mousemove', recMove, false);
  cPush();
}

function cirStart(evt){
  console.log("start cir");
  beforeCir = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add cir");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', cirMove, false);
}

function cirMove(evt) {
  
  console.log("cir move");
  ctx.putImageData(beforeCir, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.arc((startX+mousePos.x)/2, (startY+mousePos.y)/2, Math.abs( (startX-mousePos.x)/2), 0, 2 * Math.PI);
  ctx.fillStyle = clr;
  ctx.fill();
}

function cirStop(){
  console.log("end cir");
  canvas.removeEventListener('mousemove', cirMove, false);
  cPush();
}

function triStart(evt){
  console.log("start tri");
  beforeTri = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add tri");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', triMove, false);
}

function triMove(evt) {
  
  console.log("tri move");
  ctx.putImageData(beforeTri, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.moveTo(startX, startY);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.lineTo(startX - (mousePos.x-startX), mousePos.y);
  ctx.fillStyle = clr;
  ctx.fill();
}

function triStop(){
  console.log("end tri");
  canvas.removeEventListener('mousemove', triMove, false);
  cPush();
}

var savedImages = [];

function cPush() {
  console.log("push");
  var imgSorce = ctx.getImageData(0, 0, canvas.width, canvas.height);
  savedImages[++savedI] = imgSorce;
}
function cUndo() {
    if (savedI > 0) {
        console.log("undo ");
        var imgSorce = ctx.getImageData(0, 0, canvas.width, canvas.height);
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.putImageData(savedImages[--savedI], 0, 0);
    }
}
function cRedo() {
    if (savedI < savedImages.length-1) {
        console.log("redo");
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.putImageData(savedImages[++savedI], 0, 0);
    }
}

document.getElementById("redo").addEventListener('click', function() {
  cRedo();
}, false);

document.getElementById("undo").addEventListener('click', function() {
  cUndo();
}, false);

window.onload = function(){
  cPush();
}

document.getElementById("download").addEventListener('click', function() {
  var src = canvas.toDataURL();
  document.getElementById("download").href = src;
}, false);

function orecStart(evt){
  console.log("start orec");
  beforeRec = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add rec");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', orecMove, false);
}

function orecMove(evt) {
  
  console.log("rec move");
  ctx.putImageData(beforeRec, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.rect(startX, startY, mousePos.x-startX, mousePos.y-startY);
  ctx.stroke();
}

function orecStop(){
  console.log("end orec");
  canvas.removeEventListener('mousemove', orecMove, false);
  cPush();
}



function ocirStart(evt){
  console.log("start cir");
  beforeCir = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add cir");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', ocirMove, false);
}

function ocirMove(evt) {
  
  console.log("cir move");
  ctx.putImageData(beforeCir, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.arc((startX+mousePos.x)/2, (startY+mousePos.y)/2, Math.abs( (startX-mousePos.x)/2), 0, 2 * Math.PI);
  ctx.stroke();
}

function ocirStop(){
  console.log("end rec");
  canvas.removeEventListener('mousemove', ocirMove, false);
  cPush();
}



function otriStart(evt){
  console.log("start tri");
  beforeTri = ctx.getImageData(0, 0, canvas.width, canvas.height);
  console.log("add tri");
  startX = evt.clientX;
  startY = evt.clientY;
  console.log("startX : " + startX +　" startY : " + startY);
  
  canvas.addEventListener('mousemove', otriMove, false);
}

function otriMove(evt) {
  
  console.log("tri move");
  ctx.putImageData(beforeTri, 0, 0);
  var mousePos = getMousePos(canvas, evt);
  //console.log("startX : " + startX +　" startY : " + startY);
  ctx.beginPath();
  ctx.moveTo(startX, startY);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.lineTo(startX - (mousePos.x-startX), mousePos.y);
  ctx.lineTo(startX, startY);
  ctx.stroke();
}

function otriStop(){
  console.log("end tri");
  canvas.removeEventListener('mousemove', otriMove, false);
  cPush();
}

var imageLoader = document.getElementById('upload');
    imageLoader.addEventListener('change', handleImage, false);

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}