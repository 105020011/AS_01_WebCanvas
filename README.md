# Assignment_1
功能: 
1. Brush :
    可由右上方選擇筆刷顏色, 
    若選擇白色則為橡皮擦, 
    顏色下方的 scroll bar 可以選擇筆刷大小
2. Text :
    按 Text 可以輸入文字, 
    scroll bar 下方的選單可以選擇字形和大小, 
    可以在右上方選擇文字顏色
3. Cursor : 
    選擇顏色和功能時是手的形狀, 
    使用筆刷時是筆的形狀, 且會根據顏色改變筆外觀的顏色, 
    使用 text 時是 text cursor, 
    畫圖形時是鎬子形狀
4. Reset :
    顏色左邊的 reset 可 reset canvas
5. Shapes : 
    選擇 Rec, Cir, Tri 分別可畫實心的長方形, 圓形和三角形, 
    Holo Rec, Holo Cir, Holo Tri 可畫空心的長方形, 圓形和三角形, 
    可以在右上方選擇顏色, 
    空心的可以利用 scroll bar 調整線條粗細
6. Un/Re-do :
    選擇 Undo 和 Redo
7. Image :
    選擇檔案可以上傳且貼上
8. Download :
    選擇 Download Canvas 可以下載當前的 canvas, 預設名稱為 canvas

           

